from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Yudhistira Erlandinata'


# Create your views here.
def index(request):
    response = {
        'name': mhs_name,
        'age': calculate_age(1998)
    }
    return render(request, 'index.html', response)


def calculate_age(birth_year):
    return datetime.now().year - birth_year
